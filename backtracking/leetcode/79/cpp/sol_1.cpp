/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 79. Word Search
 * @date 2024.07.24
 *
 * Time Complexity: O(n*(3^m))
 * Space Complexity: O(m)
 *
 * m: Size of string to be match.
 * n: Size of nums.
 *
 * The 3-tree depth is m.
 */

#include <iostream>
#include <vector>
#include <vector>

using namespace std;

class Solution {
private:
    const vector<pair<int, int>> directions = {
        { 0,  1}, 
        { 1,  0}, 
        { 0, -1}, 
        {-1,  0}, 
    };
    bool backtrack(int i, int j, int k, vector<vector<bool>>& used, vector<vector<char>>& board, string& word) {
        if (k == word.size()) {
            return true;
        }
        if (i < 0 || i >= board.size() || j < 0 || j >= board[0].size() ||
            used[i][j] || board[i][j] != word[k]) {
            return false;
        }

        used[i][j] = true;
        for (auto& dir : directions) {
            if (backtrack(i + dir.first, j + dir.second, k + 1, used, board, word)) {
                return true;
            }
        }
        used[i][j] = false;

        return false;
    }
public:
    bool exist(vector<vector<char>>& board, string word) {
        string path;
        vector<vector<bool>> used(board.size(), vector<bool>(board[0].size(), false));

        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board[0].size(); j++) {
                bool found = backtrack(i, j, 0, used, board, word);
                if (found) {
                    return true;
                }
            }
        }

        return false;
    }
};

