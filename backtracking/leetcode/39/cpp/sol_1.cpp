/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 39. Combination Sum
 * @date 2024.07.24
 *
 * Time Complexity: O(n^(T/M))
 * Space Complexity: O(T/M)
 * 
 * T: Target number
 * M: Minunum number in nums
 * n: Size of nums
 * 
 * The n-tree depth is T/M.
 */

#include <iostream>
#include <vector>

using namespace std;

class Solution {
private:
    void backtrack(vector<int>& candidates, int target, vector<int>& path, vector<vector<int>>& res, int start) {
        if (target == 0) {
            res.push_back(path);
            return;
        }
        if (target < 0) {
            return;
        }
        for (int i = start; i < candidates.size(); ++i) {
            path.push_back(candidates[i]);
            backtrack(candidates, target - candidates[i], path, res, i);
            path.pop_back();
        }
    }

public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        vector<vector<int>> res;
        vector<int> path;
        backtrack(candidates, target, path, res, 0);
        return res;       
    }
};
