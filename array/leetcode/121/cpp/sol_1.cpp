/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 121. Best Time to Buy and Sell Stock
 * @date 2024.07.17
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int min = INT_MAX;
        int max_gain = 0;
        
        for (int price: prices) {
            if(min > price) {
                min = price;
            }
            else if (price - min > max_gain) {
                max_gain = price - min;
            }
        }
        return max_gain;
    }
};
