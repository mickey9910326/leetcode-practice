/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 274. H-Index
 * @date 2024.07.03
 *
 * Time Complexity: O(nlogn)
 * Space Complexity: O(logn)
 */

#include <stdlib.h>

int cmp(const void *a, const void *b) {
    return (*(int *)a - *(int *)b);
}

int hIndex(int* citations, int citationsSize) {
    if (citations == NULL || citationsSize == 0) {
        return 0;
    }

    qsort(citations, citationsSize, sizeof(int), cmp);

    for (int i = 0; i < citationsSize; i++) {
        if (citations[i] >= (citationsSize - i)) {
            return (citationsSize - i);
        }
    }
    
    return 0;
}