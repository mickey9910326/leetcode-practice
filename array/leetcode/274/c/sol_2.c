/**
 * @file sol_2.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 274. H-Index
 * @date 2024.07.03
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 * 
 * Use arr to storage each times of citations.
 * And visit arr to get h-index.
 */

#include <stdlib.h>
#include <string.h>

int hIndex(int* citations, int citationsSize) {
    int* arr;
    int sum;

    if (citations == NULL || citationsSize == 0) {
        return 0;
    }

    arr = malloc(sizeof(int) * (citationsSize + 1));
    memset(arr, 0, sizeof(int) * (citationsSize + 1));
    for (int i = 0; i < citationsSize; i++) {
        if (citations[i] > citationsSize) {
            arr[citationsSize]++;
        } else {
            arr[citations[i]]++;
        }
    }

    sum = 0;
    for (int i = 0; i <= citationsSize; i++) {
        sum += arr[citationsSize - i];
        if (sum >= citationsSize - i) {
            return citationsSize - i;
        }
    }

    return 0;
}