/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 977. Squares of a Sorted Array
 * @date 2024.07.02
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <stdlib.h>

int cmp(const void *a, const void *b) {
    return (*(int *)a - *(int *)b);
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* sortedSquares(int* nums, int numsSize, int* returnSize) {
    *returnSize = numsSize;
    for (int i=0; i<numsSize; i++) {
        nums[i] = nums[i]*nums[i];
    }

    qsort(nums, numsSize, sizeof(int), cmp);
    return nums;
}