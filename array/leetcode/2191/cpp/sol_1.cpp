/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 2191. Sort the Jumbled Numbers
 * @date 2024.07.24
 *
 * Time Complexity: O(n*log(n))
 * Space Complexity: O(n)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <vector>
#include <iostream>

using namespace std;

class Solution {
public:
    vector<int> sortJumbled(vector<int>& mapping, vector<int>& nums) {
        int arr[10];
        vector<pair<int, int>> pairs;

        for(int i = 0; i < 10; i++) {
            int idx = mapping[i];
            arr[i] = idx;
        }

        for (int i = 0; i < nums.size(); i++) {
            int new_num = 0;
            int num = nums[i];
            int idx;
            int digit = 1;
            do {
                idx = (num / digit) % 10;
                // cout << idx <<","<< num<<","<<digit<<endl;
                new_num += arr[idx] * digit;
                digit *= 10;
            } while (num / digit);
            // cout << num <<","<< new_num<<","<<i<<endl;
            pairs.push_back({new_num, i});
        }
        
        
        sort(pairs.begin(), pairs.end());
        vector<int> result;
        for (auto pair: pairs) {
            result.push_back(nums[pair.second]);
        }
        return result;
    }
};
