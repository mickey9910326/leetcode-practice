/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1380. Lucky Numbers in a Matrix
 * @date 2024.07.19
 *
 * Time Complexity: O(m*n)
 * Space Complexity: O(1)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <vector>
#include <iostream>

using namespace std;
class Solution {
public:
    vector<int> luckyNumbers (vector<vector<int>>& matrix) {
        int m = matrix[0].size();
        int n = matrix.size();
        int min_x;
        int max_y;
        vector<int> result;

        for (int y = 0; y < n; y++) {

            min_x = 0;
            for (int x = 1; x < m; x++) {
                if (matrix[y][x] < matrix[y][min_x]) {
                    min_x = x;
                }

            }

            max_y = y;
            for (int j = 0; j < n; j++) {
                if (matrix[j][min_x] > matrix[max_y][min_x]) {
                    max_y = j;
                    break;
                }
            }

            if (max_y == y) {
                result.push_back(matrix[max_y][min_x]);
            }
        }

        return result;
    }
};
