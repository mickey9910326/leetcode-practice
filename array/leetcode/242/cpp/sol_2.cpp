/**
 * @file sol_2.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 242. Valid Anagram
 * @date 2024.07.03
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1) (256 btyes)
 * 
 * Use only one counter.
 */

#include <string>
using namespace std;

class Solution {
public:
    bool isAnagram(string s, string t) {
        int counter[256] = {[0 ... 255] = 0};

        for (char & c: s) {
            counter[c] += 1;
        }
        for (char & c: t) {
            counter[c] -= 1;
        }

        for (int c: counter) {
            if (c != 0) {
                return false;
            }
        }
        return true;
    }
};
