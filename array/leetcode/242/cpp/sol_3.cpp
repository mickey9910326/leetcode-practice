/**
 * @file sol_3.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 242. Valid Anagram
 * @date 2024.07.03
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1) (256 btyes)
 * 
 * Use only one counter.
 */

#include <string>
#include <unordered_map>
using namespace std;

class Solution {
public:
    bool isAnagram(string s, string t) {
        unordered_map<int, int> counter;

        for (char& c: s) {
            counter[c] += 1;
        }
        for (char& c: t) {
            counter[c] -= 1;
        }

        /* C++ 98 style */
        // for (unordered_map<int, int>::const_iterator it = counter.begin();
        //      it != counter.end(); ++it) {
        //     if (it->second !=0 ) {
        //         return false;
        //     }
        // }

        /* C++ 11 style */
        for (auto& it: counter) {
            if (it.second != 0) {
                return false;
            }
        }

        return true;
    }
};
