/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 242. Valid Anagram
 * @date 2024.07.03
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1) (512 btyes)
 */

#include <string>
using namespace std;

class Solution {
public:
    bool isAnagram(string s, string t) {
        int c1[256] = {[0 ... 255] = 0};
        int c2[256] = {[0 ... 255] = 0};

        for (char & c: s) {
            c1[c] += 1;
        }
        for (char & c: t) {
            c2[c] += 1;
        }

        for (int i = 0; i < 256; i++) {
            if (c1[i] != c2[i]) {
                return false;
            }
        }
        return true;
    }
};
