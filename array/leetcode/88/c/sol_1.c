/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 88. Merge Sorted Array
 * @date 2024.07.03
 *
 * Time Complexity: O((n+m)log(n+m))
 * Space Complexity: O(1)
 */

static void insert_element(int* arr, int val, int idx, int len, int size) {
    for (int i = len + 1; i > idx; i--) {
        // printf("%d->%d\r\n", i, i-1);
        if (i >= size) {
            continue;
        } else {
            arr[i] = arr[i - 1];
        }
            // arr[i] = arr[i - 1];
    }
    arr[idx] = val;
}

static void print_arr(int* arr, int len) {
    for (int i = 0; i < len; i++) {
        printf("%3d, ", arr[i]);
    }
    printf("\r\n");
}

void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
    int i = 0;
    int j = 0;

    while (i < m && j < n) {
        if (nums1[i + j] <= nums2[j]) {
            /* move i to next */
            i++;
        }
        else {
            /* insert nums2[j], and move j to next */
            insert_element(nums1, nums2[j], i + j, m + j, nums1Size);
            j++;
        }
    }
    
    while (j < n) {
        nums1[m+j] = nums2[j];
        j++;
    }

    return;
}