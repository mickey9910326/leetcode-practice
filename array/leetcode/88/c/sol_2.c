/**
 * @file sol_2.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 88. Merge Sorted Array
 * @date 2024.07.03
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

static void insert_element(int* arr, int val, int idx, int len, int size) {
    for (int i = len + 1; i > idx; i--) {
        // printf("%d->%d\r\n", i, i - 1);
        if (i >= size) {
            continue;
        }
        else {
            arr[i] = arr[i - 1];
        }
    }
    arr[idx] = val;
}

static void print_arr(int* arr, int len) {
    for (int i = 0; i < len; i++) {
        printf("%3d, ", arr[i]);
    }
    printf("\r\n");
}

void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
    int i = m - 1;     /* read index of nums1 */
    int j = n - 1;     /* read index of nums2 */
    int k = m + n - 1; /* write index of nums1 */

    while (j >= 0) {
        if (i >= 0 && nums1[i] > nums2[j]) {
            nums1[k] = nums1[i];
            i--;
        }
        else {
            nums1[k] = nums2[j];
            j--;
        }
        k--;
    }

    return;
}