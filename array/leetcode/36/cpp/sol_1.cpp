/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 36. Valid Sudoku
 * @date 2024.07.04
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 */

#include <string>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        unordered_set<char> num_set;
        char c;

        /* rows */
        for (int i = 0; i < 9; i++) {
            num_set = {};
            for (int j = 0; j < 9; j++) {
                c = board[i][j];
                if (c == '.') {
                    /* Do nothing */
                    continue;
                }
                else if (num_set.find(c) != num_set.end()) {
                    return false;
                } else {
                    num_set.insert(c);
                }
            }
        }

        /* cols */
        for (int i = 0; i < 9; i++) {
            num_set = {};
            for (int j = 0; j < 9; j++) {
                c = board[j][i];
                if (c == '.') {
                    /* Do nothing */
                    continue;
                }
                else if (num_set.find(c) != num_set.end()) {
                    return false;
                } else {
                    num_set.insert(c);
                }
            }
        }
        
        /* blocks */
        for (int i = 0; i < 9; i++) {
            num_set = {};
            for (int j = 0; j < 9; j++) {
                int x = (i % 3) * 3 + (j % 3);
                int y = (i / 3) * 3 + (j / 3);
                c = board[x][y];
                if (c == '.') {
                    /* Do nothing */
                    continue;
                }
                else if (num_set.find(c) != num_set.end()) {
                    return false;
                } else {
                    num_set.insert(c);
                }
            }
        }

        return true;
    }
};
