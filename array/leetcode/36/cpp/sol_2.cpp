/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 36. Valid Sudoku
 * @date 2024.07.04
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 * 
 * Use bitmask
 */

#include <string>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        int mask;
        char c;

        /* rows */
        for (int i = 0; i < 9; i++) {
            mask = 0;
            for (int j = 0; j < 9; j++) {
                c = board[i][j];
                if (c == '.') {
                    /* Do nothing */
                    continue;
                }
                else if (mask & (1 << (c - '0'))) {
                    return false;
                } else {
                    mask |= (1 << (c - '0'));
                }
            }
        }

        /* cols */
        for (int i = 0; i < 9; i++) {
            mask = 0;
            for (int j = 0; j < 9; j++) {
                c = board[j][i];
                if (c == '.') {
                    /* Do nothing */
                    continue;
                }
                else if (mask & (1 << (c - '0'))) {
                    return false;
                } else {
                    mask |= (1 << (c - '0'));
                }
            }
        }
        
        /* blocks */
        for (int i = 0; i < 9; i++) {
            mask = 0;
            for (int j = 0; j < 9; j++) {
                int x = (i % 3) * 3 + (j % 3);
                int y = (i / 3) * 3 + (j / 3);
                c = board[x][y];
                if (c == '.') {
                    /* Do nothing */
                    continue;
                }
                else if (mask & (1 << (c - '0'))) {
                    return false;
                } else {
                    mask |= (1 << (c - '0'));
                }
            }
        }

        return true;
    }
};
