/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 42. Trapping Rain Water
 * @date 2024.07.06
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
    int trap(vector<int>& height) {
        int l;
        int r;

        if (height.size() <= 2) {
            return 0;
        }

        vector<int> top_l;
        vector<int> top_r;
        int max_h;
        int total_area = 0;
        int sub_area;

        /* Find top from left */
        max_h = 0;
        for (int i = 0; i < height.size(); i++) {
            if (height[i] > max_h) {
                max_h = height[i];
                top_l.push_back(i);
            }
        }

        /* Find top from right */
        max_h = 0;
        for (int i = height.size() - 1; i >= 0; i--) {
            if (height[i] > max_h) {
                max_h = height[i];
                top_r.push_back(i);
            }
        }

        /* Calculte each area from left */
        for (int i = 0; i < top_l.size() - 1; i++) {
            sub_area = height[top_l[i]] * (top_l[i + 1] - top_l[i]);
            for (int j = top_l[i]; j < top_l[i + 1]; j++) {
                sub_area -= height[j];
            }

            total_area += sub_area;
        }

        /* Calculte the area of middle */
        l = top_l.back();
        r = top_r.back();
        if (l != r) {
            sub_area = height[l] * (r - l);
            for (int j = l; j < r; j++) {
                sub_area -= height[j];
            }

            total_area += sub_area;
        }

        /* Calculte each area from right */
        for (int i = 0; i < top_r.size() - 1; i++) {
            sub_area = height[top_r[i]] * (top_r[i] - top_r[i + 1]);
            for (int j = top_r[i]; j > top_r[i + 1]; j--) {
                sub_area -= height[j];
            }

            total_area += sub_area;
        }

        return total_area;
    }
};