/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 42. Trapping Rain Water
 * @date 2024.07.06
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
    int trap(vector<int>& height) {
        int i;
        int j;
        int max_l = 0;
        int max_r = 0;
        int area = 0;

        i = 0;
        j = height.size() - 1;
        while (j > i) {
            if (height[j] >= height[i]) {
                if (height[i] > max_l) {
                    max_l = height[i];
                } else {
                    area += max_l - height[i];
                }
                i++;
            } else {
                if (height[j] > max_r) {
                    max_r = height[j];
                } else {
                    area += max_r - height[j];
                }
                j--;
            }
        }

        return area;
    }
};