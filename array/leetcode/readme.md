# Array

## Leetcode Recommand

| Num. | Title                          | Difficulty | Recommand |
|------|--------------------------------|------------|-----------|
| 350  | Intersection of Two Arrays II  | Easy       | O         |
| 1550 | Three Consecutive Odds         | Easy       | X         |
| 977  | Squares of a Sorted Array      | Easy       | X         |
| 274  | H-Index                        | Medium     | O         |
| 217  | Contains Duplicate             | Easy       | X         |
| 88   | Merge Sorted Array             | Easy       | O         |
| 242  | Valid Anagram                  | Easy       | O         |
| 1    | Two Sum                        | Easy       | O         |
| 347  | Top K Frequent Elements        | Medium     | O         |
| 271  | Encode and Decode Strings      | Medium     | X         |
| 238  | Product of Array Except Self   | Medium     | O         |
| 36   | Valid Sudoku                   | Medium     | X         |
| 167  | Two Sum II - Input Array Is Sorted | Medium | O         |
|      |                                |            |           |
|      |                                |            |           |
