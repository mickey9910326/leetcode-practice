/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 350. Intersection of Two Arrays II
 * @date 2024.07.02
 *
 * Time Complexity: O(m*n)
 * Space Complexity: O(m|n)
 * m is the nums1Size
 * n is the nums2Size
 */

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* intersect(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize) {
    int*  ret    = malloc(sizeof(int)  * nums2Size);
    bool* picked = malloc(sizeof(bool) * nums2Size);
    *returnSize = 0;

    memset(picked, 0, sizeof(bool) * nums2Size);
    for (int i = 0; i < nums1Size; i++) {
        for (int j = 0; j < nums2Size; j++) {
            if (nums1[i] == nums2[j] && !picked[j]) {
                ret[*returnSize] = nums2[j];
                (*returnSize)++;
                picked[j] = true;
                break;
            }
        }
    }

    free(picked);

    return ret;
}
