/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 2678. Number of Senior Citizens
 * @date 2024.08.01
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 */

#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    int countSeniors(vector<string>& details) {
        int cnt = 0;
        for (const string& detail : details) {
            int age = stoi(detail.substr(11, 2));
            if (age > 60) {
                cnt++;
            }
        }
        return cnt;
    }
};
