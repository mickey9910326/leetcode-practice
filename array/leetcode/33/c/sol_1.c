/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 33. Search in Rotated Sorted Array
 * @date 2024.07.10
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

int search(int* nums, int numsSize, int target) {
    int l;
    int r;
    int m;

    l = 0;
    r = numsSize - 1;

    while (l <= r) {
        m = l + (r - l) / 2;

        if (nums[m] == target) {
            return m;
        }

        if (nums[l] < nums[m]) {
            /* left side is inorder */
            if (nums[m] > target && target >= nums[l]) {
                r = m - 1;
            }
            else {
                l = m + 1;
            }
        }
        else {
            /* right side is inorder */
            if (nums[m] < target && target <= nums[r]) {
                l = m + 1;
            }
            else {
                r = m - 1;
            }
        }

    }
    return -1;
}