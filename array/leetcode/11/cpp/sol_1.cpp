/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 11. Container With Most Water
 * @date 2024.07.06
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <algorithm>
#include <vector>

using namespace std;
class Solution {
public:
    int maxArea(vector<int>& height) {
        int i;
        int j;
        int c;
        int max_c = 0;

        i = 0;
        j = height.size() - 1;
        while (j > i) {
            c = min(height[i], height[j]) * (j - i);

            if (c > max_c) {
                max_c = c;
            }

            if (height[i] > height[j]) {
                j--;
            } else {
                i++;
            }
        }

        return max_c;
    }
};
