/**
 * @file sol_2.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 347. Top K Frequent Elements
 * @date 2024.07.04
 *
 * Time Complexity: O(n log k)
 * Space Complexity: O(n)
 */

#include <algorithm>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;
typedef pair<int, int> pii;

struct _cmp {
    bool operator()(pii a, pii b) {
        return a.second < b.second;
    }
};

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {

        unordered_map<int, int> counters;
        for (int num: nums) {
            counters[num]++;
        }

        priority_queue<pii, vector<pii>, greater<pii>> min_heap;
        for (auto& c: counters) {
            min_heap.push({c.second, c.first});
            if (min_heap.size() > k) {
                min_heap.pop();
            }
        }

        vector<int> ret;
        while (!min_heap.empty()) {
            ret.push_back(min_heap.top().second);
            min_heap.pop();
        }

        return ret;
    }
};
