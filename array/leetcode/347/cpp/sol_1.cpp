/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 347. Top K Frequent Elements
 * @date 2024.07.04
 *
 * Time Complexity: O(n log n)
 * Space Complexity: O(n)
 */

#include <algorithm>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

bool _cmp(pair<int, int>& a, pair<int, int>& b) { 
    return a.second > b.second; 
}

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {

        /**
         * 1. Counting Element Frequencies
         * Time Complexity: O(n)
         * Space Complexity: O(n)
         */
        unordered_map<int, int> counters;
        for (int num: nums) {
            counters[num]++;
        }

        /**
         * 2. Transferring Count Results to a Vector
         * Time Complexity: O(n)
         * Space Complexity: O(n)
         */
        vector<pair<int, int>> f;
        for (auto& c: counters) {
            f.push_back(c);
        }

        /**
         * 3. Sorting
         * Time Complexity: O(n log n)
         * Space Complexity: O(log n)
         */
        sort(f.begin(), f.end(), _cmp);
        
        /**
         * 4. Selecting the Top K Frequent Elements
         * Time Complexity: O(k)
         * Space Complexity: O(k)
         */
        vector<int> ret;
        for (int i = 0; i < k; i++) {
            ret.push_back(f[i].first);
        }
        
        return ret;
    }
};
