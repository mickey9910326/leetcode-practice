/**
 * @file sol_3.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 347. Top K Frequent Elements
 * @date 2024.07.04
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <algorithm>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;
typedef pair<int, int> pii;
using namespace std;

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {

        /**
         * 1. Counting Element Frequencies
         * Time Complexity: O(n)
         * Space Complexity: O(n)
         */
        unordered_map<int, int> counters;
        for (int num : nums) {
            counters[num]++;
        }

        /**
         * 2. Transferring Count Results to a Vector
         * Time Complexity: O(n)
         * Space Complexity: O(n)
         */
        vector<pair<int, int>> f;
        for (const auto& entry : counters) {
            f.push_back({entry.second, entry.first});
        }

        /**
         * 3. Use Quickselect to find the k most frequent elements
         * Time Complexity: O(n) worst O(n^2)
         * Space Complexity: O(n)
         */
        quickselect(f, 0, f.size() - 1, k);

        /**
         * Step 4: Extract the top K frequent elements
         */
        vector<int> ret;
        for (int i = 0; i < k; ++i) {
            ret.push_back(f[i].second);
        }

        return ret;
    }

private:
    int partition(vector<pair<int, int>>& v, int left, int right) {
        int pivot = v[right].first;
        int i = left;
        for (int j = left; j < right; ++j) {
            if (v[j].first >= pivot) {
                swap(v[i], v[j]);
                ++i;
            }
        }
        swap(v[i], v[right]);
        return i;
    }

    void quickselect(vector<pair<int, int>>& v, int left, int right, int k) {
        if (left < right) {
            int pivotIndex = partition(v, left, right);
            if (pivotIndex == k) {
                return;
            } else if (pivotIndex > k) {
                quickselect(v, left, pivotIndex - 1, k);
            } else {
                quickselect(v, pivotIndex + 1, right, k);
            }
        }
    }
};