/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 271. Encode and Decode Strings
 * @date 2024.07.04
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <string>
#include <vector>

using namespace std;

#define STATE_HEADER_NUM    0
#define STATE_NUM           1
#define STATE_HEADER_STRING 2
#define STATE_STRING        3
#define STATE_ERROR         4

class Codec {
public:

    // Encodes a list of strings to a single string.
    string encode(vector<string>& strs) {
        string en_s;
        for (const string& s: strs) {
            en_s.append("#");
            en_s.append(to_string(s.size()));
            en_s.append("-");
            en_s.append(s);
        }
        return en_s;
    }

    // Decodes a single string to a list of strings.
    vector<string> decode(string s) {
        vector<string> ret;
        int state = STATE_HEADER_NUM;
        string str_num;
        int strlen;
        string str_element;
        string::iterator it = s.begin();

        while (it != s.end() && state != STATE_ERROR) {
            switch (state) {
            case STATE_HEADER_NUM:
                if (*it != '#') {
                    state = STATE_ERROR;
                }
                state = STATE_NUM;
                str_num = "";
                it++;
                break;
            case STATE_NUM:
                if (*it == '-') {
                    str_element = "";
                    strlen = stoi(str_num);
                    if (strlen == 0) {
                        state = STATE_HEADER_NUM;
                        ret.push_back(str_element);
                    } else {
                        state = STATE_STRING;
                    }
                } else {
                    str_num += *it;
                }
                it++;
                break;
            case STATE_STRING:
                str_element += *it;
                strlen--;
                if (strlen == 0) {
                    ret.push_back(str_element);
                    state = STATE_HEADER_NUM;
                }
                it++;
                break;
            case STATE_ERROR:
                ret = {};
                break;
            default:
                state = STATE_ERROR;
                break;
            }

        }

        return ret;
    }

};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.decode(codec.encode(strs));