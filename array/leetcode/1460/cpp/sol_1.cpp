/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1460. Make Two Arrays Equal by Reversing Subarrays
 * @date 2024.08.03
 *
 * Time Complexity: O(N)
 * Space Complexity: O(N)
 */

#include <algorithm>
#include <unordered_map>
#include <vector>

using namespace std;
class Solution {
   public:
    bool canBeEqual(vector<int>& target, vector<int>& arr) {
        if (target.size() != arr.size()) {
            return false;
        }

        unordered_map<int, int> h;
        for (int num : target) {
            h[num]++;
        }

        for (int num : arr) {
            h[num]--;
        }

        for (auto it : h) {
            if (it.second != 0) {
                return false;
            }
        }

        return true;
    }
};
 