/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1701. Average Waiting Time
 * @date 2024.07.09
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <stdlib.h>

#define ARRIVE_IDX      0    /* Index of arrive time. */
#define WORKTIME_IDX    1    /* Index of work time. */

double averageWaitingTime(int** customers, int customersSize, int* customersColSize) {
    int* waiting_time;
    int chef_time;
    int sum;
    double avg;

    if (*customersColSize != 2) {
        return 0;
    }
    else if (customersSize == 0) {
        return 0;
    }

    waiting_time = malloc(sizeof(int) * customersSize);

    chef_time = 0;
    
    for (int i = 0; i < customersSize; i++) {
        if (customers[i][ARRIVE_IDX] > chef_time) {
            chef_time = customers[i][ARRIVE_IDX];
        }
        chef_time += customers[i][WORKTIME_IDX];
        waiting_time[i] = chef_time - customers[i][ARRIVE_IDX];
    }

    sum = 0;
    for (int i = 0; i < customersSize; i++) {
        sum += waiting_time[i];
    }
    
    avg = (double)sum / customersSize;

    return avg;
}
