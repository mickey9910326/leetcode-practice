/**
 * @file sol_2.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 238. Product of Array Except Self
 * @date 2024.07.04
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int sz = nums.size();
        vector<int> ret(sz, 1);

        for (int i = 1; i < sz; i++) {
            ret[i] = ret[i - 1] * nums[i - 1];
        }

        int temp = 1;
        for (int i = sz - 1; i >= 0; i--) {
            ret[i] = ret[i] * temp;
            temp *= nums[i];
        }
        
        return ret;
    }
};

/*
1 2 3 4 5
e a b c d
d e a b c
c d e a b
b c d e a

1 2 3 4 5
e 
d e 
c d e 
b c d e 

1 2 3 4 5
  a b c d
    a b c
      a b
        a

| bcde | cde | de |  e  |   1  |
|   1  |  a  | ab | abc | abcd |
*/