/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1598. Crawler Log Folder
 * @date 2024.07.10
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <string>
#include <vector>

class Solution {
    public:
    int minOperations(vector<string>& logs) {
        int op = 0;
        for (string s : logs) {
            if (s == "../") {
                if (op > 0) {
                    op--;
                }
            }
            else if (s == "./") {
                /* do nothing */
            }
            else {
                op ++;
            }
        }
        return op;
    }
};