/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1509. Minimum Difference Between Largest and Smallest Value in Three Moves
 * @date 2024.07.03
 *
 * Time Complexity: O(nlogn)
 * Space Complexity: O(logn)
 */

#include <stdlib.h>

int cmp(const void *a, const void *b) {
    return (*(int *)a - *(int *)b);
}

int minDifference(int* nums, int numsSize) {
    int min = INT_MAX;
    int dif;

    if (numsSize <= 4) {
        return 0;
    }

    qsort(nums, numsSize, sizeof(int), cmp);

    for (int i = 0; i < 4; i++) {
        dif = nums[numsSize - 1 - (3 - i)] - nums[i];
        if (dif < min) {
            min = dif;
        }
    }

    return min;
}