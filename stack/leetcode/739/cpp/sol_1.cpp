/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 739. Daily Temperatures
 * @date 2024.07.07
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <stack>
#include <vector>

using namespace std;

class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        stack<pair<int, int>> t; /* <temperature, index> */
        vector<int> ret(temperatures.size());

        int idx = 0;
        while (idx < temperatures.size()) {
            if (t.size() == 0) {
                t.push({temperatures[idx], idx});
                idx++;
            }
            else if (temperatures[idx] > t.top().first) {
                ret[t.top().second] = idx - t.top().second;
                t.pop();
            }
            else {
                t.push({temperatures[idx], idx});
                idx++;
            }
        }

        return ret;
    }
};
