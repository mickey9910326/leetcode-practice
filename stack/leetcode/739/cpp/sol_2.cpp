/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 739. Daily Temperatures
 * @date 2024.07.07
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <stack>
#include <vector>

using namespace std;

class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        vector<int> r(temperatures.size());

        int max_t = INT_MIN;
        int cur_t = INT_MIN;
        int i = temperatures.size() - 1;
        int interval;
        while (i >= 0) {
            cur_t = temperatures[i];
            if (cur_t >= max_t) {
                max_t = cur_t;
            }
            else {
                interval = 1;
                while(cur_t >= temperatures[i + interval]) {
                    // cout << "i:" << i << ", interval:" << interval << ", t:" << endl;
                    interval += r[i + interval];
                }
                r[i] = interval;
            }
            i--;
        }

        return r;
    }
};
