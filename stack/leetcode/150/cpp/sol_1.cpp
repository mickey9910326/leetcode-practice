/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 150. Evaluate Reverse Polish Notation
 * @date 2024.07.07
 *
 * Time Complexity: O(1)
 * Space Complexity: O(n)
 */

#include <algorithm>
#include <map>
#include <stack>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        int result = 0;
        stack<int> nums;
        int a = 0;
        int b = 0;

        for (string s: tokens) {
            if (s == "+") {
                a = nums.top();
                nums.pop();
                b = nums.top();
                nums.pop();
                result = b + a;
                nums.push(result);
            }
            else if (s == "-") {
                a = nums.top();
                nums.pop();
                b = nums.top();
                nums.pop();
                result = b - a;
                nums.push(result);
            }
            else if (s == "*") {
                a = nums.top();
                nums.pop();
                b = nums.top();
                nums.pop();
                result = b * a;
                nums.push(result);
            }
            else if (s == "/") {
                a = nums.top();
                nums.pop();
                b = nums.top();
                nums.pop();
                result = b / a;
                nums.push(result);
            } else {
                cout << stoi(s)  << endl;
                nums.push(stoi(s));
            }
        }

        if (nums.size()) {
            return nums.top();
        }
        else {
            return 0;
        }
    }
};
