/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 20. Valid Parentheses
 * @date 2024.07.06
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <algorithm>
#include <map>
#include <stack>
#include <string>
#include <vector>

using namespace std;
class Solution {
public:
    bool isValid(string s) {
        stack<char> brakets;
        const map<char, char> matches = {
            {'}', '{'},
            {']', '['},
            {')', '('},
        };
        bool result = true;

        for (char c: s) {
            switch (c) {
            case '{':
            case '[':
            case '(':
                brakets.push(c);
                break;
            case '}':
            case ']':
            case ')':
                if (brakets.size() == 0) {
                    return false;
                }
                else if (matches.find(c)->second == brakets.top()) {
                    brakets.pop();
                }
                else {
                    return false;
                }
            default:
                break;
            }
        }

        if (brakets.size() > 0) {
            return false;
        }
        else {
            return true;
        }
    }
};