/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 155. Min Stack
 * @date 2024.07.07
 *
 * Time Complexity: O(1)
 * Space Complexity: O(n)
 */

#include <algorithm>
#include <map>
#include <stack>
#include <string>
#include <vector>

using namespace std;

class MinStack {
public:
    MinStack() {
    }
    
    void push(int val) {
        if (nums.size() == 0) {
            min_idxs.push_back(0);
        }
        else if (getMin() > val) {
            min_idxs.push_back(nums.size());
        }
        nums.push_back(val);
    }
    
    void pop() {
        if (nums.size() > 0) {
            if(min_idxs.back() == nums.size() - 1) {
                /* The Last One is min, remove it's idx */
                min_idxs.pop_back();
            }
            nums.pop_back();
        }
    }
    
    int top() {
        return nums.back(); 
    }
    
    int getMin() {
        return nums[min_idxs.back()];
    }
private:
    vector<int> nums;
    vector<int> min_idxs;
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(val);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */