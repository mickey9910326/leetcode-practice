/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 22. Generate Parentheses
 * @date 2024.07.07
 *
 * Time Complexity: O(4^n / (n * sqrt(n)))
 * Space Complexity: O(n)
 * 
 * Num of tree nodes is C_n (Catalan Number).
 * The C_n ~= 4^n / (n * sqrt(n)).
 */

#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    vector<string> generateParenthesis(int n) {
        vector<string> result = {};
        dfs(n, 0, 0, "", result);

        return result;
    }
private:
    void dfs(int n, int l, int r, string s, vector<string>& result) {
        // cout << "l:" << l << ", r:" << r << "s:" << s << endl;

        if (l == n && r == n) {
            result.push_back(s);
            return;
        }

        if (n > l) {
            dfs(n, l+1, r, s + '(', result);
        }
        if (n > r && l > r) {
            dfs(n, l, r+1, s + ')', result);
        }
    }
};
