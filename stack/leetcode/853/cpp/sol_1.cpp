/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 853. Car Fleet
 * @date 2024.07.07
 *
 * Time Complexity: O(n log n)
 * Space Complexity: O(n)
 */

#include <algorithm>
#include <functional>
#include <string>
#include <stack>
#include <vector>

using namespace std;

class Car{
public:
    int pos;
    int spd;
    Car(int pos, int spd);
    ~Car();
    bool operator <(const Car& c) {
        return this->pos < c.pos;
    }
    bool operator >(const Car& c) {
        return this->pos > c.pos;
    }
};

Car::Car(int pos, int spd) {
    this->pos = pos;
    this->spd = spd;
}

Car::~Car()
{
}


class Solution {
public:
    int carFleet(int target, vector<int>& position, vector<int>& speed) {
        vector<Car> cars;

        for (int i = 0; i < position.size(); i++) {
            cars.push_back(Car(position[i], speed[i]));
        }

        sort(cars.begin(), cars.end());

        stack<float> arrivet;
        float t;
        for (auto car: cars) {
            t = (target - car.pos) / (float)car.spd;
            while(!arrivet.empty() && t >= arrivet.top()) {
                arrivet.pop();
            }
            arrivet.push(t);
        }

        return arrivet.size();
    }
};