/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 2134. Minimum Swaps to Group All 1's Together II
 * @date 2024.08.02
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 */

#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    int minSwaps(vector<int>& nums) {
        int z_cnt = 0;
        int o_cnt = 0;
        int c;
        int n = nums.size();
        int min_swap = INT_MAX;

        for (int i = 0; i < n; i++) {
            if (nums[i] == 0) {
                z_cnt += 1;
            }
        }

        o_cnt = n - z_cnt;

        /* Count steps for grouping 1 together */
        c = 0;
        for (int i = 0; i < n; i++) {
            if (i >= o_cnt) {
                min_swap = min(min_swap, c);

                if (nums[i - o_cnt] == 0) {
                    c--;
                }
            }

            if (nums[i] == 0) {
                c++;
            }
        }

        /* Count steps for grouping 0 together */
        c = 0;
        for (int i = 0; i < n; i++) {
            if (i >= z_cnt) {
                min_swap = min(min_swap, c);

                if (nums[i - z_cnt] == 1) {
                    c--;
                }
            }

            if (nums[i] == 1) {
                c++;
            }
        }

        return min_swap;
    }
};
