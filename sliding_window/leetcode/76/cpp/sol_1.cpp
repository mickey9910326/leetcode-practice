/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 76. Minimum Window Substring
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;

class Solution {
public:
    string minWindow(string s, string t) {
        if (t.length() > s.length() || t.length() == 0) {
            return "";
        }

        int l = 0;
        int r = 0;
        int hash_t[256];
        int hash_s[256];
        int min_len = INT_MAX;
        int min_l = 0;
        int min_r = 0;

        memset(hash_t, 0, 256 * sizeof(int));
        memset(hash_s, 0, 256 * sizeof(int));

        for (char c: t) {
            hash_t[c] += 1;
        }

        while (r < s.length()) {
            char rc = s[r];
            hash_s[rc] += 1;
            // cout << rc << ":" << hash_s[rc] << endl;

            while (cmp(hash_s, hash_t, 256)) {
                if (min_len > r - l + 1) {
                    min_len = r - l + 1;
                    min_l = l;
                    min_r = r;
                    // cout << "min_len:" << min_len << ", min_l:" << min_l << ", min_r:" << min_r << endl;
                }

                char lc = s[l];
                hash_s[lc] -= 1;

                // cout << rc << ":" << hash_s[rc] << ", " << lc << ":" << hash_s[lc] << endl;
                l++;
            }

            r++;
        }

        if (min_len == INT_MAX) {
            return "";
        }

        string sub = s.substr(min_l, min_len);
        return sub;
    }
private:
    bool cmp(const int* h1, const int* h2, int size) {
        for (int i = 0; i < size; i++) {
            if (h1[i] < h2[i]) {
                return false;
            }
        }
        return true;
    }
};
