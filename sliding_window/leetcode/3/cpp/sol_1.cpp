/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 3. Longest Substring Without Repeating Characters
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        /* All charater in s are undercase */
        int counter[256];
        memset(counter, 0, 256 * sizeof(int));

        int l = 0;
        int r = 0;
        int max_len = 0;

        while (r < s.length()) {
            char rc = s[r];
            counter[rc] += 1;
            // cout << s[r] << counter[rc]<<endl;

            while (counter[rc] > 1) {
                char lc = s[l];
                counter[lc] -= 1;
                // cout << s[r] << counter[rc] << s[l]<<counter[lc] <<endl;
                l++;
            }

            max_len = max(max_len, r - l + 1);
            r++;
        }
        
        return max_len;
    }
};