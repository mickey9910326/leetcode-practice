/**
 * @file sol_2.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 382. Linked List Random Node
 * @date 2024.07.02
 *
 * Time Complexity: O(1)
 * Space Complexity: O(1)
 *
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

#include <stdlib.h>

typedef struct {
    struct ListNode* head;
    int len;
} Solution;

Solution* solutionCreate(struct ListNode* head) {
    Solution* p = NULL;
    struct ListNode* node;
    p = malloc(sizeof(Solution));
    if (p) {
        p->head = head;
        p->len  = 0;

        node = head;
        while(node) {
            p->len++;
            node = node->next;
        }
    }
    return p;
}

int solutionGetRandom(Solution* obj) {
    int r;
    struct ListNode* node;

    r = (rand() % obj->len);
    node = obj->head;
    while (r != 0) {
        r--;
        node = node->next;
    }

    return node->val;
}

void solutionFree(Solution* obj) {
    free(obj);
}

/**
 * Your Solution struct will be instantiated and called as such:
 * Solution* obj = solutionCreate(head);
 * int param_1 = solutionGetRandom(obj);
 
 * solutionFree(obj);
 */
