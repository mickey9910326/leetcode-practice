/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 382. Linked List Random Node
 * @date 2024.07.02
 *
 * Time Complexity: O(1)
 * Space Complexity: O(1)
 *
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

#include <stdlib.h>

typedef struct {
    struct ListNode* head;
} Solution;

Solution* solutionCreate(struct ListNode* head) {
    Solution* p = NULL;
    p = malloc(sizeof(Solution));
    if (p) {
        p->head = head;
    }
    return p;
}

int solutionGetRandom(Solution* obj) {
    int r = rand();
    int len = 0;
    struct ListNode* node;

    node = obj->head;
    while (r != 0 && node) {
        r--;
        len++;
        node = node->next;
    }

    if (r == 0) {
        return node->val;
    }

    r = r % len;
    node = obj->head;

    while (r != 0) {
        r--;
        node = node->next;
    }


    return node->val;
}

void solutionFree(Solution* obj) {
    free(obj);
}

/**
 * Your Solution struct will be instantiated and called as such:
 * Solution* obj = solutionCreate(head);
 * int param_1 = solutionGetRandom(obj);
 
 * solutionFree(obj);
*/