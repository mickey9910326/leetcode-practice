/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1598. Crawler Log Folder
 * @date 2024.07.10
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicatesUnsorted(ListNode* head) {
        unordered_map<int, int> counter;
        ListNode* curr;
        ListNode* prev;
        ListNode Sentinel(0, head);

        curr = head;
        while(curr) {
            cout << "->" << curr;
            counter[curr->val]++;
            curr = curr->next;
        }
            cout << endl;

        prev = &Sentinel;
        curr = head;
        while(curr) {
            cout << prev->val << "->" << curr->val << endl;
            if(counter[curr->val] >= 2) {
                curr = curr->next;
                prev->next = curr;
                delete prev->next;
            } else {
                prev = curr;
                curr = curr->next;
            }
        }

        return Sentinel.next;
    }
};