/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 2487. Three Consecutive Odds
 * @date 2024.07.03
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <stdlib.h>
#include <stdint.h>

void print_list(struct ListNode* head) {
    while(head) {
        printf("%10d->", head->val);
        head = head->next;
    }
    printf("NULL\r\n");
}

struct ListNode* removeNodes(struct ListNode* head) {
    struct ListNode sentinel = {.next = head, .val = INT_MAX};
    struct ListNode* prev;
    struct ListNode* curr;
    struct ListNode* next;

    if (head == NULL) {
        return head;
    }
    else if (head->next == NULL) {
        return head;
    }

    /**
     * Break to two list
     * List A: in order
     * List B: val to compare
     */
    prev = &sentinel;
    curr = head;
    next = head->next;
    curr->next = prev;
    prev->next = NULL;

    while(next) {
        if (curr->val < next->val) {
            // free(curr);  // NOTE: Must free curr in real case
            curr = prev;
            prev = prev->next;
        } else {
            prev = curr;
            curr = next;
            next = next->next;
            curr->next = prev;
        }
    }

    // reverse link list
    while(prev) {        
        curr->next = next;
        next = curr;
        curr = prev;
        prev = prev->next;
    }
    curr->next = next;
    // print_list(&sentinel);
    
    return sentinel.next;
}

/**
 * List A: in order
 * List B: val to compare
 * Case 1: if N <= C,
 *    Move N to List A as Head.
 *    Move all node to next of original list.
 * Case 2: if N  > C, move all to next
 *    Remove C
 *    Move C & P node to next in List A.
 * 
 * Intput: 
 * INT_MAX -> 05 -> 02 -> 13 -> 03 -> 08
 *
 * NULL <- INT_MAX <- 05    02 -> 13 -> 03 -> 08
 *               P <-  C     N
 *
 * NULL <- INT_MAX <- 05 <- 02    13 -> 03 -> 08
 *               P <-  C     N
 *
 * NULL <- INT_MAX <- 05    13 -> 03 -> 08
 *               P <-  C     N
 *
 * NULL <- INT_MAX <- 05    13 -> 03 -> 08 -> NULL
 *               P <-  C     N
 *
 * NULL <- INT_MAX    13 -> 03 -> 08 -> NULL
 *    P <-       C     N
 * 
 * NULL <- INT_MAX <- 13    03 -> 08 -> NULL
 *               P <-  C     N
 *
 * NULL <- INT_MAX <- 13 <- 03    08 -> NULL
 *                     P <-  C     N
 *
 * NULL <- INT_MAX <- 13 <- 08    NULL
 *                     P <-  C     N
 * 
 * and reverse list from node C
 */