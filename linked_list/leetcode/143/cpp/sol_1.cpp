/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 143. Reorder List
 * @date 2024.08.07
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 */

class Solution {
public:
    void reorderList(ListNode* head) {
        ListNode* p = head;
        ListNode* q = head;

        /* Find Mid */ 
        while (q && q->next) {
            p = p->next;
            q = q->next->next;
        }
        

        ListNode* prev = nullptr;
        ListNode* curr = p->next;
        ListNode* next;

        /* Reverse right side */
        while (curr) {
            next = curr->next;
            curr->next = prev;
            prev = curr;
            curr = next;
        }
        
        ListNode* l = head;
        ListNode* r = prev;
        /* Make last node of left side list become NULL */
        p->next = nullptr;

        /* Insert right side list to left list */
        while (l && r) {
            next = l->next;
            l->next = r;
            r = r->next;
            l->next->next = next;
            l = next;
        }
    }
};
