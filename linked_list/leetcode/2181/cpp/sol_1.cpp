/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 2181. Merge Nodes in Between Zeros
 * @date 2024.07.04
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 */

#include <string>
#include <unordered_set>
#include <vector>


class Solution {
public:
    ListNode* mergeNodes(ListNode* head) {
        
        if (head == NULL) {
            return NULL;
        } else if (head->next == NULL) {
            return NULL;
        }

        ListNode* prev = head;
        ListNode* curr = head->next;
        int val;
        while (1) {
            if (curr->val == 0) {
                prev = curr;
                curr = curr->next;
            }
            else {
                prev->val += curr->val;
                curr = curr->next;
                delete prev->next;
                prev->next = curr;
            }

            if (curr->next == NULL) {
                prev->next = NULL;
                delete curr;
                break;
            }
        }

        return head;
    }
};