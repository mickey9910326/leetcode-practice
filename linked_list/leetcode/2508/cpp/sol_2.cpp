/**
 * @file sol_2.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 2058. Find the Minimum and Maximum Number of Nodes Between Critical Points
 * @date 2024.07.05
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <algorithm>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    vector<int> nodesBetweenCriticalPoints(ListNode* head) {
        if (head == NULL) {
            return {-1, -1};
        } else if (head->next == NULL) {
            return {-1, -1};
        } else if (head->next->next == NULL) {
            return {-1, -1};
        }

        int f_idx = 0; /* The first index of critical point */
        int l_idx = 0; /* The last index of critical point */
        int p_idx = 0; /* Previous index of critical point */
        int idx   = 1; /* Current index of node */

        int max_d = INT_MIN;
        int min_d = INT_MAX;
        ListNode* p = head;
        ListNode* q = head->next->next;
        while (q) {
            bool cp_found = (p->val > p->next->val && p->next->val < q->val) |
                            (p->val < p->next->val && p->next->val > q->val);

            if (cp_found) {
                if (p_idx == 0) {
                    f_idx = idx;
                } else {
                    min_d = min(min_d, idx - p_idx);
                }
                l_idx = idx;
                p_idx = idx;
            }

            idx++;
            p = p->next;
            q = q->next;
        }
        
        if (min_d == INT_MAX) {
            return {-1, -1};
        }

        max_d = l_idx - f_idx;

        return {min_d, max_d};
    }
};
