/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 2058. Find the Minimum and Maximum Number of Nodes Between Critical Points
 * @date 2024.07.05
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <algorithm>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    vector<int> nodesBetweenCriticalPoints(ListNode* head) {
        if (head == NULL) {
            return {-1, -1};
        } else if (head->next == NULL) {
            return {-1, -1};
        } else if (head->next->next == NULL) {
            return {-1, -1};
        }

        int idx = 0;
        vector<int> cp_pos;
        ListNode* prev = head;
        ListNode* curr = head->next;
        ListNode* next = head->next->next;
        while (next) {
            idx++;
            if (next->val > curr->val && prev->val > curr->val) {
                cp_pos.push_back(idx);
            }
            else if (next->val < curr->val && prev->val < curr->val) {
                cp_pos.push_back(idx);
            }
            prev = curr;
            curr = next;
            next = next->next;
        }

        if (cp_pos.size() < 2) {
            return {-1, -1};
        }

        int max = cp_pos.back() - cp_pos.front();
        int min = max;
        for (int i = 0; i < cp_pos.size() - 1; i++) {
            if (min > (cp_pos[i + 1] - cp_pos[i])) {
                min = cp_pos[i + 1] - cp_pos[i];
            }
        }

        return {min, max};
    }
};
