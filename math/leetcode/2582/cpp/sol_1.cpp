/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 2582. Pass the Pillow
 * @date 2024.07.06
 *
 * Time Complexity: O(1)
 * Space Complexity: O(1)
 */

using namespace std;
class Solution {
public:
    int passThePillow(int n, int time) {
        int mod = time % (n + n - 2);
        int p;

        if (mod < n) {
            p = mod + 1;
        } else {
            p = 2*n - mod - 1;
        }

        return p;
    }
};
