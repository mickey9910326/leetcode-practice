/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1518. Water Bottles
 * @date 2024.07.07
 *
 * Time Complexity: O(1)
 * Space Complexity: O(1)
 */

using namespace std;

class Solution {
public:
    int numWaterBottles(int numBottles, int numExchange) {
        int result = numBottles;

        if (numExchange == 0) {
            return 0;
        }

        while (numBottles >= numExchange) {
            result += numBottles / numExchange;
            numBottles = (numBottles / numExchange) + (numBottles % numExchange);
        }

        return result;
    }
};