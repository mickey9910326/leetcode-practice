/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1823. Find the Winner of the Circular Game
 * @date 2024.07.08
 *
 * Time Complexity: O(n*k)
 * Space Complexity: O(n)
 */

#include <vector>

using namespace std;

class Solution {
public:
    int findTheWinner(int n, int k) {
        vector<bool> v(n);
        int times = n - 1;
        int idx = 0;
        int t = 0;

        idx = 0;
        t = 0;
        while (times) {
            t += (v[idx] ? 0 : 1);
            if (t == k) {
                v[idx] = true;
                times--;
                t = 0;
            }
            idx = (idx + 1) % n;
        }

        for (idx = 0; idx < n; idx++) {
            if (v[idx] == false) {
                return idx + 1;
            }
        }

        return -1;
    }
};
