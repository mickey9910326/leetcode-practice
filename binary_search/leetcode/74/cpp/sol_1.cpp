/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 74. Search a 2D Matrix
 * @date 2024.07.07
 *
 * Time Complexity: O(log(m*n))
 * Space Complexity: O(1)
 */

#include <vector>

using namespace std;

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        if (matrix.size() == 0) {
            return false;
        }

        int m = matrix.size();
        int n = matrix[0].size();
        int x;
        int y;
        int l = 0;
        int r = m * n - 1;
        int pre_idx = 0;
        int idx = m * n / 2;
        bool result = false;

        while (l <= r) {
            idx = (l + r) / 2;
            x = idx % m;
            y = idx / m;
            if (matrix[y][x] == target) {
                result = true;
                break;
            }
            else if (matrix[y][x] > target) {
                r = idx - 1;
            }
            else {
                l = idx + 1;
            }
        }

        return result;
    }
};