/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 875. Koko Eating Bananas
 * @date 2024.07.08
 *
 * Time Complexity: O(n * log(m))
 * Space Complexity: O(1)
 */

#include <algorithm>
#include <vector>

using namespace std;


class Solution {
public:
    int minEatingSpeed(vector<int>& piles, int h) {
        int l = 0;
        int r = *max_element(piles.begin(), piles.end());
        int k;
        int time;

        while (l < r) {
            k = (l + r) / 2;
            
            /* Caculate time */
            time = 0;
            for (int i = 0; i < piles.size(); i++) {
                if (piles[i] > 0) {
                    time += (piles[i] / k) + ((piles[i] % k) ? 1 : 0);
                }
            }

            if (time > h) {
                l = k + 1;
            }
            else {
                r = k;
            }
        }

        return r;
    }
};