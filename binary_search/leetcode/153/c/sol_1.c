/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 153. Find Minimum in Rotated Sorted Array
 * @date 2024.07.09
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

/**
 * Case 1. l > r && l > m && m > r:
 *    Min in the right side.
 *    3 4 5 6 7 1 2
 *    l     m     r
 * Case 2. l > r && l > m && m < r:
 *    Min in the left side.
 *    6 7 1 2 3 4 5
 *    l     m     r
 * Case 3. l < r && l < m && m < r:
 *    Min in the left side.
 *    1 2 3 4 5 6 7
 *    l     m     r
 */

/**
 * Devide to 2 case
 * 
 * Case 1.  m > r:
 *    Min in the right side.
 * Case 2.  m < r:
 *    Min in the left side.
 */

#include <stdlib.h>

int findMin(int* nums, int numsSize) {
    int l = 0;
    int r = numsSize - 1;
    int m;

    while (l < r) {
        m = l + (r - l) / 2;

        if (nums[m] > nums[r]) {
            l = m + 1;
        } else {
            r = m;
        }
    }

    return nums[l];
}
