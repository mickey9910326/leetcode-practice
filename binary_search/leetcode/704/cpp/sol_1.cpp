/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief 704. Binary Search
 * @date 2024.07.07
 *
 * Time Complexity: O(log(n))
 * Space Complexity: O(1)
 */

#include <vector>

using namespace std;

class Solution {
public:
    int search(vector<int>& nums, int target) {
        int l = 0;
        int r = nums.size() - 1;
        int idx;

        while (l <= r) {
            idx = (l + r) / 2;
            if (target == nums[idx]) {
                break;
            }
            else if (target < nums[idx]) {
                r = idx - 1;
            }
            else {
                l = idx + 1;
            }
        }

        if (target != nums[idx]) {
            idx = -1;
        }

        return idx;
    }
};
