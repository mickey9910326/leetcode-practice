# Tree

## Leetcode Recommand

| Num. | Title                               | Difficulty | Recommand |
|------|-------------------------------------|------------|-----------|
| 988  | Smallest String Starting From Leaf  | Medium     | O         |
|      |                                     |            |           |
