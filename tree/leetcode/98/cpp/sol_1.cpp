/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 98. Validate Binary Search Tree
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(D)
 */

#include <queue>

using namespace std;

class Solution {
public:
    bool isValidBST(TreeNode* root) {
        TreeNode* prev = nullptr;
        return dfs(root, prev);
    }
private:
    bool dfs(TreeNode* root, TreeNode*& prev) {
        if (root == nullptr) {
            return true;
        }

        /* Left */
        bool l = dfs(root->left, prev);
        if (l == false) {
            return false;
        }

        /* Middle */
        if (prev != nullptr && root->val <= prev->val) {
            return false;
        }

        /* right */
        prev = root;
        bool r = dfs(root->right, prev);
        return r;
    }
};
