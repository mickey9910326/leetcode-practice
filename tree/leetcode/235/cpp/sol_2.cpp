/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 235. Lowest Common Ancestor of a Binary Search Tree
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;

class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        TreeNode* n = root;

        while(n) {
            if (p->val > n->val && q->val > n->val) {
                n = n->right;
            }
            else if (p->val < n->val && q->val < n->val) {
                n = n->left;
            }
            else {
                return n;
            }
        }

        return nullptr;
    }
};
