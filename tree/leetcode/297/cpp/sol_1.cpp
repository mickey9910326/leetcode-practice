/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 297. Serialize and Deserialize Binary Tree
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(D)
 */

#include <string>
#include <vector>

using namespace std;

class Codec {
public:

    // Encodes a tree to a single string.
    string serialize(TreeNode* root) {
        string s = dfs(root);
        // cout << s << endl;
        return s;
    }

    // Decodes your encoded data to tree.
    TreeNode* deserialize(string data) {
        vector<TreeNode*> nodes = split(data);
        int idx = 0;        
        return dfs2(nodes, idx);
    }
private:
    string dfs(TreeNode* root) {
        if (root == nullptr) {
            return "#,";
        }

        string s = "";
        s += to_string(root->val);
        s += ",";
        s += dfs(root->left);
        s += dfs(root->right);
        return s;
    }


    vector<TreeNode*> split(const string& data) {
        vector<TreeNode*> nodes;
        string token;
        for (char c : data) {
            if (c == ',') {
                if (token == "#") {
                    nodes.push_back(nullptr);
                } else if (!token.empty()) {
                    int val = stoi(token);
                    TreeNode* node = new TreeNode(val);
                    nodes.push_back(node);
                }
                token.clear();
            } else {
                token += c;
            }
        }
        return nodes;
    }

    TreeNode* dfs2(const vector<TreeNode*>& nodes, int& idx) {
        if (idx >= nodes.size() || nodes[idx] == nullptr) {
            idx++;
            return nullptr;
        }

        TreeNode* root = nodes[idx];
        idx++;
        root->left = dfs2(nodes, idx);
        root->right = dfs2(nodes, idx);
        return root;
    }
};

// Your Codec object will be instantiated and called as such:
// Codec ser, deser;
// TreeNode* ans = deser.deserialize(ser.serialize(root));
