/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1110. Delete Nodes And Return Forest
 * @date 2024.07.18
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

/**
 * 1. Find node and its parent
 * 2. Split to different forest and push to result
 */
class Solution {
public:
    vector<TreeNode*> delNodes(TreeNode* root, vector<int>& to_delete) {
        unordered_set<TreeNode*> result;
        vector<TreeNode*> v;
        TreeNode* c;
        TreeNode* p;
        int dir;
        bool found;

        result.insert(root);

        for (int val: to_delete) {
            for (TreeNode* r: result) {
                bool found = dfs(r, val, &c, &p, &dir);
                if (found) {
                    break;
                }
            }

            if (c && p) {
                if (dir == 0) {
                    p->left = NULL;
                }
                else {
                    p->right = NULL;
                }
            }

            if (c) {
                if (c->left) {
                    result.insert(c->left);
                }
                if (c->right) {
                    result.insert(c->right);
                }
                auto it = result.find(c);
                if (it != result.end()) {
                    result.erase(it);

                }
            }
        }

        for (auto r: result) {
            v.push_back(r);
        }
        return v;
    }
private:
    bool dfs(TreeNode* node, int target, TreeNode** c, TreeNode** p, int* dir) {
        if (node == nullptr) {
            *p = nullptr;
            *c = nullptr;
            return false;
        }

        if (node->val == target) {
            *c = node;
            return true;
        }

        *p = node;
        *dir = 0;
        if (dfs(node->left, target, c, p, dir)) {
            return true;
        }
        *p = node;
        *dir = 1;
        if (dfs(node->right, target, c, p, dir)) {
            return true;
        }

        return false;
    }
};
