/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 543. Diameter of Binary Tree
 * @date 2024.07.19
 *
 * Time Complexity: O(n)
 * Space Complexity: O(D)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int diameterOfBinaryTree(TreeNode* root) {
        int diameter = 0;
        dfs(root, &diameter);
        return diameter;
    }
private:
    /**
     * Maxinum distance must be a path through a root.
     * So the max length is left height + right height + root length (1)
     * 
     * Calculate each root distance and compare.
     * Use return val to height of left and right.
     */
    int dfs(TreeNode* root, int* diameter) {
        if (root == nullptr) {
            return 0;
        }

        int l = dfs(root->left, diameter);
        int r = dfs(root->right, diameter);

        *diameter = max(*diameter, l + r);

        return max(l, r) + 1;
    }
};