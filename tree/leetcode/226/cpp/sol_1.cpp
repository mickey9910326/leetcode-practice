/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 226. Invert Binary Tree
 * @date 2024.07.19
 *
 * Time Complexity: O(n)
 * Space Complexity: O(D)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* invertTree(TreeNode* root) {
        dfs(root);
        return root;
    }
private:
    void dfs(TreeNode* root) {
        TreeNode* tmp;

        if (root == nullptr) {
            return;
        }

        tmp         = root->right;
        root->right = root->left;
        root->left  = tmp;

        dfs(root->left);
        dfs(root->right);

        return;
    }
};
