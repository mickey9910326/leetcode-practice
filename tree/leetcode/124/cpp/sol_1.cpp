/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 124. Binary Tree Maximum Path Sum
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(D)
 */

using namespace std;

class Solution {
public:
    int maxPathSum(TreeNode* root) {
        int max_sum = root->val;
        dfs(root, &max_sum);
        return max_sum;
    }
private:
    int dfs(TreeNode* root, int* max_sum) {
        if (root == nullptr) {
            return 0;
        }
        
        int l = max(dfs(root->left, max_sum), 0);
        int r = max(dfs(root->right, max_sum), 0);

        /* Spilted max sum */
        *max_sum = max(*max_sum, l + r + root->val);

        /* Return not spilted max sum */
        return root->val + max(l, r);
    }
};
