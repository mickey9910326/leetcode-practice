/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 230. Kth Smallest Element in a BST
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(D)
 */

using namespace std;

class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        int n = k;
        return dfs(root, &n);
    }
private:
    int dfs(TreeNode* root, int* k) {
        if (root == NULL) {
            return -1;
        }

        int l = dfs(root->left, k);
        if (l != -1) {
            return l;
        }
        // cout << *k << " " << root->val << endl;

        if (*k == 0) {
            return root->val;
        }
        *k -= 1;

        int r = dfs(root->right, k);
        return r;
    }
};
