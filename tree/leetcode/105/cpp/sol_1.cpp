/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 105. Construct Binary Tree from Preorder and Inorder Traversal
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(D)
 */

#include <vector>

using namespace std;

class Solution {
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        int idx = 0;
        return dfs(preorder, inorder, 0, inorder.size() - 1, &idx);
    }
private:
    TreeNode* dfs(vector<int>& preorder, vector<int>& inorder, int l, int r, int* idx) {
        if (l > r) {
            return nullptr;
        }

        /* Find value in this side*/
        int m;
        for (m = l; m <= r; m++) {
            if (inorder[m] == preorder[*idx]) {
                break;
            }
        }

        /* Value not found in this side*/
        if (m == r + 1) {
            return nullptr;
        }

        TreeNode* root = new TreeNode(preorder[*idx]);
        *idx += 1;
        root->left  = dfs(preorder, inorder, l, m - 1, idx);
        root->right = dfs(preorder, inorder, m + 1, r, idx);

        return root;
    }
};
