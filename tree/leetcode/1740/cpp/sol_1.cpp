/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1740. Find Distance in a Binary Tree
 * @date 2024.07.17
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <vector>
#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int findDistance(TreeNode* root, int p, int q) {
        int duplicated;
        int distance;
        string p_path;
        string q_path;

        findPath(root, p, p_path);
        findPath(root, q, q_path);

        duplicated = 0;
        while (duplicated < p_path.length() && duplicated < q_path.length() &&
               duplicated < p_path.length() &&
               p_path[duplicated] == q_path[duplicated]) {
            duplicated++;
        }

        distance = p_path.length() + q_path.length() - (duplicated * 2);

        return distance;
    }
private:
    bool findPath(TreeNode* root, int val, string& s) {
        if (root == NULL) {
            return false;
        }

        if (root->val == val) {
            return true;
        }

        s.push_back('L');
        if (findPath(root->left, val, s)) {
            return true;
        }
        s.pop_back();

        s.push_back('R');
        if (findPath(root->right, val, s)) {
            return true;
        }
        s.pop_back();

        return false;
    }
};
