/**
 * @file sol_1.c
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leeetcode 1302. Deepest Leaves Sum
 * @date 2021.05.30
 *
 * Time Complexity: O(n)
 * Space Complexity: O(log(n))
 *
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

void helper(struct TreeNode* root, int curlvl, int* maxlvl, int* sum) {
    if (root == NULL) {
        return;
    }
    helper(root->left, curlvl + 1, maxlvl, sum);
    helper(root->right, curlvl + 1, maxlvl, sum);

    if (curlvl > (*maxlvl)) {
        *sum = 0;
        *maxlvl = curlvl;
    }
    if (curlvl == (*maxlvl)) {
        *sum += root->val;
    }
}

int deepestLeavesSum(struct TreeNode* root) {
    int sum = 0;
    int maxlvl = 0;

    helper(root, 0, &maxlvl, &sum);

    return sum;
}
