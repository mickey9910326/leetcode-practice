/**
 * @file sol_2.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 572. Subtree of Another Tree
 * @date 2024.07.19
 *
 * Time Complexity: O(n + m)
 * Space Complexity: O(n + m)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isSubtree(TreeNode* root, TreeNode* subRoot) {
        string tree1 = serialize(root);
        string tree2 = serialize(subRoot);
        return tree1.find(tree2) != string::npos;
    }
    
private:
    string serialize(TreeNode* node) {
        if (node == nullptr) {
            return "#";
        }
        return "^" + to_string(node->val) + "," + serialize(node->left) + "," + serialize(node->right);
    }
};
