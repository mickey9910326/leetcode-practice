/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 2096. Step-By-Step Directions From a Binary Tree Node to Another
 * @date 2024.07.17
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <vector>
#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    string getDirections(TreeNode* root, int startValue, int destValue) {
        string ss;
        string ds;
        string dir;
        int duplicated;

        findNode(root, startValue, ss);
        findNode(root, destValue,  ds);

        if (ss.length() == 0) {
            return ds;
        }

        duplicated = 0;
        while (duplicated < ss.length() && duplicated < ds.length() &&
               ss[duplicated] == ds[duplicated]) {
            duplicated++;
        }

        for (int i = 0; i < ss.length() - duplicated; i++) {
            dir += "U";
        }

        for (int i = duplicated; i < ds.length(); i++) {
            dir += ds[i];
        }

        return dir;
    }
private:
    bool findNode(TreeNode* root, int val, string& s) {
        string dir;

        if (root == nullptr) {
            return false;
        }

        if (root->val == val) {
            return true;
        }

        s += "L";
        if (findNode(root->left, val, s)) {
            return true;
        }
        s.pop_back();

        s += "R";
        if (findNode(root->right, val, s)) {
            return true;
        }
        s.pop_back();

        return false;
    }
};
