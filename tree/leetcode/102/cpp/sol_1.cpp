/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 102. Binary Tree Level Order Traversal
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(D)
 */

#include <queue>
#include <vector>

using namespace std;

class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> result;
        queue<TreeNode*> q;
        TreeNode* n;
        int level;
        
        if (root == nullptr) {
            return result;
        }

        q.push(root);
        level = 0;
        while (q.size()) {
            result.push_back({});
            int sz = q.size();

            for (int i = 0; i < sz; i++) {
                n = q.front();
                q.pop();
                result[level].push_back(n->val);

                if (n->left) {
                    q.push(n->left);
                }
                if (n->right) {
                    q.push(n->right);
                }
            }

            level++;
        }
        return result;
    }
};