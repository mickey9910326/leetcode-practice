/**
 * @file sol_2.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1530. Number of Good Leaf Nodes Pairs
 * @date 2024.07.18
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n + D)
 * 
 * D: max depth
 * L: num of leafs
 */

#include <vector>
#include <algorithm>

using namespace std;

// struct TreeNode {
//     int val;
//     TreeNode *left;
//     TreeNode *right;
//     TreeNode() : val(0), left(nullptr), right(nullptr) {}
//     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// };

class Solution {
public:
    int countPairs(TreeNode* root, int distance) {
        int result = 0;
        dfs(root, distance, result);
        return result;
    }

private:
    vector<int> dfs(TreeNode* node, int distance, int& result) {
        if (!node) return vector<int>(distance + 1, 0);
        if (!node->left && !node->right) {
            vector<int> leafCount(distance + 1, 0);
            leafCount[1] = 1;  // Leaf node is at distance 1 from itself
            return leafCount;
        }

        vector<int> leftCounts = dfs(node->left, distance, result);
        vector<int> rightCounts = dfs(node->right, distance, result);

        // Count pairs between left and right subtrees
        for (int i = 1; i <= distance; ++i) {
            for (int j = 1; j + i <= distance; ++j) {
                result += leftCounts[i] * rightCounts[j];
            }
        }

        // Combine counts for the current node
        vector<int> nodeCounts(distance + 1, 0);
        for (int i = 1; i < distance; ++i) {
            nodeCounts[i + 1] = leftCounts[i] + rightCounts[i];
        }

        return nodeCounts;
    }
};