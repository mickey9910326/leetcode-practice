/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1530. Number of Good Leaf Nodes Pairs
 * @date 2024.07.18
 *
 * Time Complexity: O(n^2*D)
 *      Worst: O(n^2*D)
 *      Avg. : O(L^2*D)
 * Space Complexity: O(n*D)
 * 
 * D: max depth
 * L: num of leafs
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <map>
#include <iostream>

using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int countPairs(TreeNode* root, int distance) {
        vector<string> leafs;
        int num = 0;
        int d;

        findLeafs(root, &leafs, "");
        
        // for (auto leaf: leafs) {
        //     cout << leaf << endl;
        // }

        for (int i = 0; i < leafs.size() - 1; i++) {
            for (int j = i + 1; j < leafs.size(); j++) {
                d = findDistance(leafs[i], leafs[j]);
                if (d <= distance) {
                    num++;
                }
            }
        }
        

        return num;
    }
private:
    void findLeafs(TreeNode* node, vector<string>* leafs, string path) {
        if (node == nullptr) {
            return;
        }

        if (node->left == nullptr && node->right == nullptr) {
            leafs->push_back(path);
        }

        path.push_back('L');
        findLeafs(node->left, leafs, path);
        path.pop_back();

        path.push_back('R');
        findLeafs(node->right, leafs, path);
        path.pop_back();

        return;
    }
    int findDistance(string path1, string path2) {
        int max_dup = min(path1.length(), path2.length());
        int dup = 0;
        while (path1[dup] == path2[dup] && dup < max_dup) {
            dup++;
        }
        return path1.length() + path2.length() - dup*2;
    }
};
