/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 110. Balanced Binary Tree
 * @date 2024.07.19
 *
 * Time Complexity: O(n*log(n))
 * Space Complexity: O(D)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isBalanced(TreeNode* root) {
        return dfs(root);
    }
private:
    int depth(TreeNode* root) {
        if (root == nullptr) {
            return 0;
        }

        int l = depth(root->left) + 1;
        int r = depth(root->right) + 1;
        int max_d = max(l, r);
        
        return max_d;
    }
    bool dfs(TreeNode* root) {
        if (root == nullptr) {
            return true;
        }

        int lh = depth(root->left);
        int rh = depth(root->right);
        bool l = true;
        bool r = true;

        if (lh > 2) {
            l = dfs(root->left);
        }

        if (rh > 2) {
            r = dfs(root->right);
        }

        if (abs(lh - rh) >= 2) {
            return false;
        }
        else {
            return l & r;
        }
    }
};