/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 323. Number of Connected Components in an Undirected Graph
 * @date 2024.07.30
 *
 * Time Complexity: O(N + M)
 * Space Complexity: O(N + M)
 * 
 * N: numCourses
 * M: size of prerequisites
 *
 * Use DFS
 */

#include <vector>
#include <unordered_map>
#include <queue>

using namespace std;

class Solution {
public:
    int countComponents(int n, vector<vector<int>>& edges) {
        unordered_map<int, vector<int>> graph;

        for (const auto& edge : edges) {
            graph[edge[0]].push_back(edge[1]);
            graph[edge[1]].push_back(edge[0]);
        }

        vector<bool> visited(n);
        int cnt = 0;
        for (int i = 0; i < n; i++) {
            if (visited[i] == false) {
                dfs(i, visited, graph);
                cnt++;
            }
        }
        
        return cnt;
    }

private:
    void dfs(int node, vector<bool>& visited, unordered_map<int, vector<int>>& graph) {
        if (visited[node] == true) {
            return;
        }

        visited[node] = true;
        for (int neighbor : graph[node]) {
            dfs(neighbor, visited, graph);
        }

        return;
    }
};
