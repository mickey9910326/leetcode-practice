/**
 * @file sol_2.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 207. Course Schedule
 * @date 2024.07.30
 *
 * Time Complexity: O(N + M)
 * Space Complexity: O(N + M)
 * 
 * N: numCourses
 * M: size of prerequisites
 *
 * Use DFS
 */

#include <vector>
#include <unordered_map>
#include <queue>

using namespace std;

class Solution {
public:
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        unordered_map<int, vector<int>> graph;
        for (const auto& it: prerequisites) {
            int cur = it[0];
            int pre = it[1];
            graph[pre].push_back(cur);
        }

        // States: 0 = unvisited, 1 = visiting, 2 = visited
        vector<int> state(numCourses, 0);

        for (int i = 0; i < numCourses; ++i) {
            if (state[i] == 0) {
                if (hasCycle(i, graph, state)) {
                    return false;
                }
            }
        }
        
        return true;
    }

private:
    bool hasCycle(int node, const unordered_map<int, vector<int>>& graph, vector<int>& state) {
        if (state[node] == 1) { // Found a cycle
            return true;
        }
        if (state[node] == 2) { // Already visited this node
            return false;
        }

        state[node] = 1; // Mark as visiting

        if (graph.find(node) != graph.end()) {
            for (int neighbor : graph.at(node)) {
                if (hasCycle(neighbor, graph, state)) {
                    return true;
                }
            }
        }

        state[node] = 2; // Mark as visited
        return false;
    }
};