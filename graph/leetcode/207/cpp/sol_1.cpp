/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 207. Course Schedule
 * @date 2024.07.30
 *
 * Time Complexity: O(N + M)
 * Space Complexity: O(N + M)
 * 
 * N: numCourses
 * M: size of prerequisites
 * 
 * Use Kahn's algorithm
 */

#include <vector>
#include <unordered_map>
#include <queue>

using namespace std;

class Solution {
public:
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        unordered_map<int, vector<int>> graph;
        vector<int> inorder(numCourses);

        for (const auto& it: prerequisites) {
            int cur = it[0];
            int pre = it[1];

            graph[pre].push_back(cur);
            inorder[cur] += 1;
        }

        queue<int> q;
        for (int i = 0; i < numCourses; i++) {
            if (inorder[i] == 0) {
                q.push(i);
            }
        }
        
        int visited = 0;
        while (!q.empty()) {
            int course = q.front();
            q.pop();
            visited++;

            for (int n : graph[course]) {
                inorder[n]--;
                if (inorder[n] == 0) {
                    q.push(n);
                }
            }
        }
        
        return visited == numCourses;
    }
};