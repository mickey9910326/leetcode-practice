/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 133. Clone Graph
 * @date 2024.07.29
 *
 * Time Complexity: O(N + M)
 * Space Complexity: O(N)
 * 
 * M: Maxinum num of neighbors.
 * N: Number of nodes.
 */

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};
*/

#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
    Node* cloneGraph(Node* node) {
        unordered_map<Node*, Node*> m;
        dfs(node, m);
        return m[node];
    }
private:
    void dfs(Node* node, unordered_map<Node*, Node*>& map) {
        if (node == NULL) {
            return;
        }

        Node* cpy_node = new Node(node->val);
        map[node] = cpy_node;

        for(auto neighbor : node->neighbors) {
            if (map.find(neighbor) == map.end()) {
                dfs(neighbor, map);
            }
            cpy_node->neighbors.push_back(map[neighbor]);
        }
    }
};