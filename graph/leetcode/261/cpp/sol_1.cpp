/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 261. Graph Valid Tree
 * @date 2024.07.30
 *
 * Time Complexity: O(N + M)
 * Space Complexity: O(N + M)
 * 
 * N: number of nodes
 * M: number of edges
 * 
 * Use Kahn's algorithm
 */

#include <queue>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
public:
    bool validTree(int n, vector<vector<int>>& edges) {
        if (n - 1 != edges.size()) {
            return false; // A valid tree must have exactly n-1 edges
        }
        if (n == 1 && edges.size() == 0) {
            return true;
        }

        unordered_map<int, vector<int>> graph;
        vector<int> indegree(n, 0);

        // Build the graph
        for (const auto& edge : edges) {
            graph[edge[0]].push_back(edge[1]);
            graph[edge[1]].push_back(edge[0]);
            indegree[edge[0]] += 1;
            indegree[edge[1]] += 1;
        }

        // Initialize the queue with all nodes of indegree 1 (leaves)
        queue<int> q;
        for (int i = 0; i < n; i++) {
            if (indegree[i] == 1) {
                q.push(i);
            }
        }

        int visited = 0;
        while (!q.empty()) {
            int v = q.front();
            q.pop();
            visited++;

            for (int neighbor : graph[v]) {
                indegree[neighbor] -= 1;
                if (indegree[neighbor] == 1) {
                    q.push(neighbor);
                }
            }
        }

        return visited == n;
    }
};
