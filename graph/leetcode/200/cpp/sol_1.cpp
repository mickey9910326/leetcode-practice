/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 200. Number of Islands
 * @date 2024.07.29
 *
 * Time Complexity: O(n*m)
 * Space Complexity: O(n*m)
 */

#include <vector>

using namespace std;

class Solution {
public:
    int numIslands(vector<vector<char>>& grid) {
        int cnt = 0;
        for (int x = 0; x < grid.size(); x++) {
            for (int y = 0; y < grid[0].size(); y++) {
                if (dfs(x, y, grid)) {
                    cnt++;
                }
            }
        }

        return cnt;
    }
private:
    const vector<pair<int, int>> directions = {
        { 1,  0},
        { 0,  1},
        {-1,  0},
        { 0, -1},
    };

    int dfs(int x, int y, vector<vector<char>>& grid) {
        if (x < 0 || x >= grid.size() || y < 0 || y >= grid[0].size()) {
            return 0;
        }

        if (grid[x][y] == '0') {
            return 0;
        }

        int size = 1;
        grid[x][y] = '0';

        for(auto dir: directions) {
            size += dfs(x + dir.first, y + dir.second, grid);
        }

        return size;
    }
};
