/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 417. Pacific Atlantic Water Flow
 * @date 2024.07.29
 *
 * Time Complexity: O(N * M)
 * Space Complexity: O(N * M)
 * 
 * M: Maxinum num of neighbors.
 * N: Number of nodes.
 */

#include <vector>

using namespace std;

class Solution {
public:
    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
        int rows = heights.size();
        int cols = heights[0].size();
        vector<vector<int>> records(rows, vector<int>(cols, 0));
        vector<vector<int>> result;

        for (int i = 0; i < rows; i++) {
            dfs(i,        0, heights, records, 0x01);
            dfs(i, cols - 1, heights, records, 0x02);
        }
        for (int i = 0; i < cols; i++) {            
            dfs(       0, i, heights, records, 0x01);
            dfs(rows - 1, i, heights, records, 0x02);
        }

        // for (int i = 0; i < rows; i++) {
        //     for (int j = 0; j < cols; j++) {
        //         cout << records[i][j] << ",";
        //         if (records[i][j] == 3) {
        //             result.push_back({i, j});
        //         }
        //     }
        //     cout << endl;
        // }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (records[i][j] == 3) {
                    result.push_back({i, j});
                }
            }
        }

        return result;
    }
private:
    const vector<pair<int, int>> directions = {
        { 1,  0},
        { 0,  1},
        {-1,  0},
        { 0, -1},
    };

    void dfs(int x, int y, vector<vector<int>>& heights, vector<vector<int>>& records, int ocean) {
        if (records[x][y] & ocean) {
            return;
        }
        records[x][y] |= ocean;

        for (auto& dir: directions) {
            int nx = x + dir.first;
            int ny = y + dir.second;

            if (nx < 0 || nx >= heights.size() || ny < 0 || ny >= heights[0].size()) {
                /* Out of boundary, skip this point */
                continue;
            }

            if (heights[nx][ny] >= heights[x][y]) {
                dfs(nx, ny, heights, records, ocean);
            }
        }
    }
};