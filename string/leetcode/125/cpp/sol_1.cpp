/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 125. Valid Palindrome
 * @date 2024.07.05
 *
 * Time Complexity: O(n)
 * Space Complexity: O(1)
 */

#include <algorithm>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using namespace std;

bool is_alphanumeric(char c) {
    bool ret = false;
    if(c >= 'A' && c <= 'Z') {
        ret = true;
    }
    else if (c >= 'a' && c <= 'z') {
        ret = true;
    }
    else if (c >= '0' && c <= '9') {
        ret = true;
    }
    return ret;
}

char to_lowercase(char c) {
    char ret;
    if(c >= 'A' && c <= 'Z') {
        ret = c - 'A' + 'a';
    } else {
        ret = c;
    }
    return ret;
}

class Solution {
public:
    bool isPalindrome(string s) {
        int i = 0;
        int j = s.size() - 1;
        char c1;
        char c2;
        bool ret = true;

        while (j > i) {
            c1 = s[i];
            c2 = s[j];

            if (!is_alphanumeric(c1)) {
                i++;
            }
            else if (!is_alphanumeric(c2)) {
                j--;
            }
            else if (to_lowercase(c1) == to_lowercase(c2)) {
                i++;
                j--;
            }
            else {
                ret = false;
                break;
            }
        }

        return ret;
    }
};

