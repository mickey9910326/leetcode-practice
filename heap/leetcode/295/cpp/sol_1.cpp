/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 295. Find Median from Data Stream
 * @date 2024.07.20
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;

class MedianFinder {
public:
    MedianFinder() {
        
    }
    
    void addNum(int num) {
        if (nums.empty()) {
            nums.push_back(num);
        }
        else {
            nums.insert(lower_bound(nums.begin(), nums.end(), num), num);
        }
    }

    double findMedian() {
        int m = nums.size() / 2;
        if (nums.size() % 2) {
            return nums[m];
        }
        else {
            return (float)(nums[m] + nums[m -1]) * 0.5;
        }
    }
private:
    vector<int> nums;
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder* obj = new MedianFinder();
 * obj->addNum(num);
 * double param_2 = obj->findMedian();
 */