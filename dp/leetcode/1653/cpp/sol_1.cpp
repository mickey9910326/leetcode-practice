/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1653. Minimum Deletions to Make String Balanced
 * @date 2024.07.30
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 */

#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
    int minimumDeletions(string s) {
        int cnt_b = 0;
        int min_d = 0;

        for (char c : s) {
            if (c == 'b') {
                cnt_b++;
            }
            else {
                min_d = min(min_d + 1, cnt_b);
            }
        }

        return min_d;
    }
};
