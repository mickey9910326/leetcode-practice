/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 647. Palindromic Substrings
 * @date 2024.08.01
 *
 * Time Complexity: O(N^2)
 * Space Complexity: O(1)
 */

#include <string>

using namespace std;

class Solution {
public:
    int countSubstrings(string s) {
        int n = s.length();
        int cnt = 0;

        for (int m = 0; m < n; m++) {
            int l = m, r = m;
            while (l >= 0 && r < n && s[l] == s[r]) {
                cnt++;
                l--;
                r++;
            }

            l = m, r = m + 1;
            while (l >= 0 && r < n && s[l] == s[r]) {
                cnt++;
                l--;
                r++;
            }
        }

        return cnt;
    }
};
