/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1395. Count Number of Teams
 * @date 2024.07.29
 *
 * Time Complexity: O(n^2)
 * Space Complexity: O(1)
 */

#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    int numTeams(vector<int>& rating) {
        int cnt = 0;

        for (int j = 0; j < rating.size(); j++) {
            int ls = 0; /* Smaller number in the left side */
            int lb = 0; /* Bigger  number in the left side */
            int rs = 0; /* Smaller number in the right side */
            int rb = 0; /* Bigger  number in the right side */

            /* left */
            for (int i = 0; i < j; i++) {
                if (rating[i] > rating[j]) {
                    lb++;
                }
                if (rating[i] < rating[j]) {
                    ls++;
                }
            }

            /* Right */            
            for (int k = j + 1; k < rating.size(); k++) {
                if (rating[j] > rating[k]) {
                    rs++;
                }
                if (rating[j] < rating[k]) {
                    rb++;
                }
            }

            cnt += lb * rs + ls * rb;
        }

        return cnt;
    }
};