/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1395. Count Number of Teams
 * @date 2024.07.29
 *
 * Time Complexity: O(n^3)
 * Space Complexity: O(n^2)
 */

#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    int numTeams(vector<int>& rating) {
        int cnt = 0;

        vector<pair<int, int>> v;
        for (int i = 0; i < rating.size(); i++) {
            for (int j = i + 1; j < rating.size(); j++) {
                if (rating[i] > rating[j]) {
                    v.push_back({i, j});
                }
            }
        }

        for (int i = 0; i < v.size(); i++) {
            int idx_1 = v[i].second;
            for (int j = i + 1; j < v.size(); j++) {
                int idx_2 = v[j].first;
                if (idx_1 == idx_2) {
                    cnt += 1;
                }
            }
        }
        
        v.clear();
        for (int i = 0; i < rating.size(); i++) {
            for (int j = i + 1; j < rating.size(); j++) {
                if (rating[i] < rating[j]) {
                    v.push_back({i, j});
                }
            }
        }

        for (int i = 0; i < v.size(); i++) {
            int idx_1 = v[i].second;
            for (int j = i + 1; j < v.size(); j++) {
                int idx_2 = v[j].first;
                if (idx_1 == idx_2) {
                    cnt += 1;
                }
            }
        }
        
        return cnt;
    }
};