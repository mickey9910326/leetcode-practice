/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 322. Coin Change
 * @date 2024.08.01
 *
 * Time Complexity: O(N*M)
 * Space Complexity: O(M)
 * 
 * N: Number of coins
 * M: The amount
 */

#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    int coinChange(vector<int>& coins, int amount) {
        int m = amount + 1;
        int n = coins.size();
        vector<int> dp(m, m);

        dp[0] = 0;
        for (int i = 1; i <= amount; i++) {
            for (int j = 0; j < coins.size(); j++) {
                if (i - coins[j] >= 0) {
                    dp[i] = min(dp[i], 1 + dp[i - coins[j]]);
                }
            }
        }

        return dp[amount] > amount ? -1 : dp[amount];
    }
};
