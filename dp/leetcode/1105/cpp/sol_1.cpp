/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1105. Filling Bookcase Shelves
 * @date 2024.07.31
 *
 * Time Complexity: O(N^2)
 * Space Complexity: O(N)
 */

#include <vector>

using namespace std;

class Solution {
public:
    int minHeightShelves(vector<vector<int>>& books, int shelfWidth) {
        int n = books.size();
        vector<int> dp(n + 1, 0);

        dp[0] = 0;
        dp[1] = books[0][1];

        for (int i = 1; i < n; i++) {
            int w = books[i][0];
            int h = books[i][1];
            dp[i + 1] = dp[i] + h;

            // cout << "i:" << i <<"------------"<<dp[i]<<","<<dp[i+1]<<endl;

            int j = i - 1;
            while (j >= 0 && w + books[j][0] <= shelfWidth) {
                w += books[j][0];
                h = max(h, books[j][1]);
                dp[i + 1] = min(dp[i + 1], h + dp[j]);
                // cout<<j<<", w:"<<w<<", h:"<<h<<", dp:"<<dp[i+1]<<endl;
                j--;
            }
        }

        return dp[n];
    }
};
