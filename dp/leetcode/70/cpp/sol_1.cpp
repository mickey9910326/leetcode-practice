/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 70. Climbing Stairs
 * @date 2024.07.31
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 */

#include <vector>

using namespace std;

class Solution {
public:
    int climbStairs(int n) {
        int x = 1;
        int y = 1;
        for (int i = 1; i < n; i++) {
            y = y + x;
            x = y - x;
        }
        return y;
    }
};