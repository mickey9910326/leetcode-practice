/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 139. Word Break
 * @date 2024.08.03
 *
 * Time Complexity: O(N*N*K + M*K, N*K^2 + M*K)
 * Space Complexity: O(N + M * K)
 * 
 * N: Length of s
 * M: Nums of wordDict
 * K: Avg. length of word in wordDict
 */

#include <string>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        int n = s.length();
        unordered_set<string> wordSet(wordDict.begin(), wordDict.end());
        vector<bool> dp(n + 1, false);

        dp[0] = true;
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j < i; j++) {
                if (dp[j]) {
                    bool found = (wordSet.find(s.substr(j, i - j)) != wordSet.end());
                    if (found) {
                        dp[i] = true;
                        break;
                    }
                }
            }
        }

        return dp[n];
    }
};
