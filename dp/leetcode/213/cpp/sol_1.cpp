/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 198. House Robber II
 * @date 2024.07.31
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 */

#include <vector>

using namespace std;

class Solution {
public:
    int rob(vector<int>& nums) {
        int n = nums.size();
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return nums[0];
        }

        int m1 = rob_helper(nums, 0, nums.size() - 1);
        int m2 = rob_helper(nums, 1, nums.size());

        return max(m1, m2);
    }
private:
    int rob_helper(vector<int>& nums, int s, int e) {
        int prev1 = 0;
        int prev2 = 0;

        for (int i = s; i < e; i++) {
            int m = max(prev1, prev2 + nums[i]);
            prev2 = prev1;
            prev1 = m;
        }
        return prev1;
    }
};
