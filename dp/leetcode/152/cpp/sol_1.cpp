/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 152. Maximum Product Subarray
 * @date 2024.08.01
 *
 * Time Complexity: O(N)
 * Space Complexity: O(1)
 */

#include <string>
#include <vector>

#define MAX(x, y) ((x > y) ? x : y)
#define MIN(x, y) ((x < y) ? x : y)

// get maximum or minimum of 3 elements
#define MAX_3E(x, y, z) (MAX(x, MAX(y, z)))
#define MIN_3E(x, y, z) (MIN(x, MIN(y, z)))

using namespace std;

class Solution {
public:
    int maxProduct(vector<int>& nums) {
        double max = 1;
        double min = 1;
        double tmp = 1;
        double res = INT32_MIN;

        for (int i = 0; i < nums.size(); i++) {
            tmp = max * nums[i];
            max = MAX_3E(nums[i], max * nums[i], min * nums[i]);
            min = MIN_3E(nums[i], min * nums[i], tmp);
            res = MAX(res, max);
        }

        return res;
    }
};