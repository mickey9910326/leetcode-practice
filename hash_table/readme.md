# Hash

## Leetcode Recommand

| Num. | Title                          | Difficulty | Recommand |
|------|--------------------------------|------------|-----------|
| 49   | Group Anagrams                 | Medium     | O         |
| 128  | Longest Consecutive Sequence   | Medium     | O         |
|      |                                |            |           |
|      |                                |            |           |
|      |                                |            |           |
