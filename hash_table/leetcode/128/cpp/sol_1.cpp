/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 128. Longest Consecutive Sequence
 * @date 2024.07.05
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <algorithm>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using namespace std;


class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        unordered_set<int> s(nums.begin(), nums.end());

        int longestStreak = 0;
        for (int num : s) {
            if (!s.count(num - 1)) {
                int currentNum = num;
                int currentStreak = 1;
                while (s.count(currentNum + 1)) {
                    currentNum += 1;
                    currentStreak += 1;
                }
                longestStreak = max(longestStreak, currentStreak);
            }
        }
        return longestStreak;
    }
};