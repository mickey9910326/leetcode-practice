
## 請幫我解釋這段程式碼

```cpp
/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 49. Group Anagrams
 * @date 2024.07.03
 *
 * Time Complexity: O(n*L)
 * Space Complexity: O(n*L)
 * 
 * n is nums of string ()
 * L is avg. length of every string
 */

#include <unordered_map>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        if (strs.size() == 0) { // 如果輸入的字串陣列為空，則返回空的結果
            return {};
        }
        
        unordered_map<string, vector<string>> map; /* <字串的哈希值, 字串群組> */
        int counters[26]; /* 只處理小寫字母 */
        string key;

        for (string s: strs) { // 對於每個輸入的字串
            fill(begin(counters), end(counters), 0); // 重置計數器
            for (char& c: s) { // 計算每個字母出現的次數
                counters[c - 'a']++;
            }

            key = ""; // 初始化哈希鍵
            for (int i = 0; i < 26; i++) { // 生成字串的哈希鍵
                if (counters[i] > 0) {
                    key += (char)(i + 'a'); // 加上字母
                    key += to_string(counters[i]); // 加上字母出現的次數
                }
            }

            if (map.find(key) != map.end()) { // 如果哈希鍵已存在，將字串加入對應的群組
                map[key].push_back(s);
            } else { // 如果哈希鍵不存在，創建一個新的群組
                map[key] = {s};
            }
        }

        vector<vector<string>> ret; // 最後的結果
        for (auto& it: map) { // 將每個群組加入結果中
            ret.push_back(it.second);
        }

        return ret; // 返回結果
    }
};
```

簡單來說，這段程式碼的作用是將一組字串按照字母異位詞（即字母組合相同，但順序不同的字詞）分組。它首先計算每個字串中每個字母出現的次數，然後生成一個唯一的哈希鍵來代表這個字母組合。使用這些哈希鍵來將字母異位詞分組，最後返回這些分組的結果。

## 有更好的寫法嗎?

另一種更為簡潔且常見的寫法是對每個字串進行排序，然後使用排序後的字串作為鍵值來分組。這樣可以避免計數器和手動生成哈希鍵的步驟，代碼會更加簡潔且易於理解。以下是改進後的寫法：

```cpp
#include <unordered_map>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> map; /* <sorted string, words group> */

        for (string s : strs) {
            string key = s;
            sort(key.begin(), key.end()); // 對字串進行排序
            map[key].push_back(s); // 根據排序後的字串進行分組
        }

        vector<vector<string>> ret; // 最後的結果
        for (auto& it : map) {
            ret.push_back(it.second); // 將每個群組加入結果中
        }

        return ret; // 返回結果
    }
};
```

這段程式碼的具體說明如下：

1. 建立一個 `unordered_map` ，鍵為排序後的字串，值為原字串的群組。
2. 對於每個輸入的字串：
   - 將字串進行排序。
   - 使用排序後的字串作為鍵，將原字串加入對應的群組中。
3. 將所有的字串群組從 map 中提取出來，放入結果向量 ret 中。
4. 返回結果向量 ret。

這樣的寫法不僅簡潔，而且容易理解，並且能夠很好地解決字母異位詞分組的問題。

## 請比較兩種方法在時間與空間複雜度

### 方法一：使用計數器生成哈希鍵

#### 時間複雜度

對於每個字串，我們需要計算每個字母的出現次數，這需要 O(L) 的時間，其中 L 是字串的長度。
生成哈希鍵需要 O(26) 的時間（常數時間，因為字母表的大小固定）。
將字串插入哈希表中需要 O(1) 的時間。
因此，總時間複雜度為 O(n * L)，其中 n 是字串數量，L 是平均字串長度。

#### 空間複雜度

我們需要 O(n) 的空間來存儲哈希表中的所有鍵值對。
每個鍵的長度最多為 O(L)，因為每個字母和其頻率都會佔用空間。
因此，總空間複雜度為 O(n * L)。

### 方法二：對字串進行排序

#### 時間複雜度

對於每個字串，我們需要對其進行排序，這需要 O(L log L) 的時間。
將字串插入哈希表中需要 O(1) 的時間。
因此，總時間複雜度為 O(n * L log L)。

#### 空間複雜度

我們需要 O(n) 的空間來存儲哈希表中的所有鍵值對。
每個鍵的長度為 O(L)，因為排序後的字串長度為 L。
因此，總空間複雜度為 O(n * L)。


### 比較

#### 時間複雜度

方法一的時間複雜度為 O(n * L)，而方法二的時間複雜度為 O(n * L log L)。
因此，方法一在時間複雜度上略勝一籌，尤其是當字串較長時，方法二的排序操作會更耗時。
空間複雜度

兩種方法的空間複雜度都是 O(n * L)，因為都需要儲存哈希表中的鍵值對。

### 結論
 - 方法一（使用計數器生成哈希鍵）在時間複雜度上稍微更優，特別是在處理長字串時。然而，這種方法的實現相對較為複雜。
 - 方法二（對字串進行排序）雖然在時間複雜度上稍遜一籌，但實現更為簡潔直觀，更容易理解和實施，且在實際應用中通常能夠提供足夠好的性能。

因此，選擇哪種方法主要取決於具體情況和對代碼可讀性的要求。如果追求最高效的時間複雜度，且不介意稍微複雜一些的實現，可以選擇方法一。如果更重視代碼簡潔和可讀性，方法二是更好的選擇。
