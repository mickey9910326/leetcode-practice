/**
 * @file sol_1.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 49. Group Anagrams
 * @date 2024.07.03
 *
 * Time Complexity: O(n*L)
 * Space Complexity: O(n*L)
 * 
 * n is nums of string ()
 * L is avg. length of every string
 */

#include <unordered_map>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        if (strs.size() == 0) {
            return {};
        }
        
        unordered_map<string, vector<string>> map; /* <hash for word, words group> */
        int counters[26]; /* only lowercase letters */
        string key;

        for (string s: strs) {
            /* Calculate the nums of each latter */
            fill(begin(counters), end(counters), 0);
            for (char& c: s) {
                counters[c - 'a']++;
            }

            /* Gen the hash of string */
            key = "";
            for (int i = 0; i < 26; i++) {
                if (counters[i] > 0) {
                    key += (char)(i + 'a');
                    key += to_string(counters[i]);
                }
            }

            /* Push string to group */
            if (map.find(key) != map.end()) {
                map[key].push_back(s);
            } else {
                map[key] = {s};
            }
        }

        /* Take out group from map, push in to ret */
        vector<vector<string>> ret;
        for (auto& it: map) {
            ret.push_back(it.second);
        }

        return ret;
    }
};