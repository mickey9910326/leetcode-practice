/**
 * @file sol_3.cpp
 * @author LiYu87 (mickey9910326@gmail.com)
 * @brief leetcode 1. Two Sum
 * @date 2020.07.03
 *
 * use hash table
 * more clear code
 *
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 */

#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
   public:
    vector<int> twoSum(vector<int>& nums, int target) {
        std::unordered_map<int, int> map; /* <diff, index> */
        int diff;

        for(int i = 0; i < nums.size(); ++i) {
            diff = target - nums[i];
            if (map.find(diff) != map.end()) {
                return {i, map[diff]};
            }
            map[nums[i]] = i;
        }

        return {};
    }
};
